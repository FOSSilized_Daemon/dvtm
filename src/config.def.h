#include "tile.c"
#include "grid.c"
#include "bstack.c"
#include "fullscreen.c"

/***********
 * General *
 ***********/
/* Scroll back buffer size (in lines). */
#define SCROLL_HISTORY 1000

/**************
 * Appareance *
 **************/
/* Status bar (command line option -s) position.
 *     BAR_BOTTOM: Bar will be drawn at bottom of dvtm(1).
 *     BAR_TOP: Bar will be drawn at top of dvtm(1).*/
#define BAR_POS BAR_BOTTOM

/* Automatically Hide status bar.
 *     TRUE: Bar will be hidden when only one window exists.
 *     FALSE: Bar will show even when only one window exists. */
#define BAR_AUTOHIDE FALSE

/* Characters for beginning and end of status bar message. */
#define BAR_BEGIN '['
#define BAR_END   ']'

/* Color scheme definitions.
 * Valid curses attributes:
 *     A_NORMAL        Normal display (no highlight)
 *     A_STANDOUT      Best highlighting mode of the terminal.
 *     A_UNDERLINE     Underlining
 *     A_REVERSE       Reverse video
 *     A_BLINK         Blinking
 *     A_DIM           Half bright
 *     A_BOLD          Extra bright or bold
 *     A_PROTECT       Protected mode
 *     A_INVIS         Invisible or blank mode */
enum {
	DEFAULT,
	GREY42,
	CYAN1,
	RED1,
};

static Color colors[] = {
	[DEFAULT] = { .fg = -1,  .bg = -1, .fg256 = -1, .bg256 = -1, },
 	[GREY42]  = { .fg = 242, .bg = -1, .fg256 = 242, .bg256 = -1, },
 	[CYAN1]   = { .fg = 51,  .bg = -1, .fg256 = 51,  .bg256 = -1, },
 	[RED1]    = { .fg = 196, .bg = -1, .fg256 = 196, .bg256 = -1, },
 };

#define COLOR(c) COLOR_PAIR(colors[c].pair)

/* Curses attributes for the currently focused window. */
#define SELECTED_ATTR (COLOR(CYAN1) | A_NORMAL)

/* Curses attributes for normal (not selected) windows. */
#define NORMAL_ATTR (COLOR(GREY42) | A_DIM)

/* Curses attributes for a window with pending urgent flag. */
#define URGENT_ATTR (COLOR(RED1) | A_STANDOUT)

/* Curses attributes for the status bar. */
#define BAR_ATTR (COLOR(CYAN1) | A_NORMAL)

/* Curses attributes for the currently selected tags. */
#define TAG_SEL (COLOR(CYAN1) | A_BOLD)

/* Curses attributes for not selected tags which contain no windows. */
#define TAG_NORMAL (COLOR(GREY42) | A_DIM)

/* Curses attributes for not selected tags which contain windows. */
#define TAG_OCCUPIED (COLOR(GREY42) | A_NORMAL)

/* Curses attributes for not selected tags which with urgent windows. */
#define TAG_URGENT (COLOR(RED1) | A_NORMAL)

/********
 * Tags *
 ********/
/* List of tag names. */
const char tags[][8] = { "1", "2", "3", "4", "5" };

/* The printf(3) format string for the tag in the status bar. */
#define TAG_SYMBOL "[%s]"

/***********
 * Layouts *
 ***********/
/* Factor the master area size (how much of the screen the master window takes
 * up).
 *     Range: 0.1-0.9 */
#define MFACT 0.5

/* Total number of clients in the master area. */
#define NMASTER 1

/* List of layouts (NOTE: The first entry is the default layout).
 * The available layouts are as following:
 *    - tile
 *    - grid
 *    - bstack
 *    - fullscreen */
static Layout layouts[] = {
	{ "[]=", tile },
	{ "+++", grid },
	{ "TTT", bstack },
	{ "[ ]", fullscreen },
};

/****************
 * Key-bindings *
 ****************/
/* Modifier key definitions. */
#define MOD  CTRL('a')
#define TAGKEYS(KEY,TAG) \
{ { MOD, 'v', KEY,     }, { view,           { tags[TAG] }               } }, \
{ { MOD, 't', KEY,     }, { tag,            { tags[TAG] }               } }, \
{ { MOD, 'V', KEY,     }, { toggleview,     { tags[TAG] }               } }, \
{ { MOD, 'T', KEY,     }, { toggletag,      { tags[TAG] }               } },

/* you can at most specifiy MAX_ARGS (3) number of arguments */
static KeyBinding bindings[] = {
	/*  modifier       key                 function             argument (MAX_ARGS of 3) */
	{ { MOD,           'c',           }, { create,            { NULL }               } },
	{ { MOD,           'C',           }, { create,            { NULL, NULL, "$CWD" } } },
	{ { MOD,           'x', 'x',      }, { killclient,        { NULL }               } },
	{ { MOD,           'j',           }, { focusnext,         { NULL }               } },
	{ { MOD,           'J',           }, { focusnextnm,       { NULL }               } },
	{ { MOD,           'K',           }, { focusprevnm,       { NULL }               } },
	{ { MOD,           'k',           }, { focusprev,         { NULL }               } },
	{ { MOD,           'f',           }, { setlayout,         { "[]=" }              } },
	{ { MOD,           'g',           }, { setlayout,         { "+++" }              } },
	{ { MOD,           'b',           }, { setlayout,         { "TTT" }              } },
	{ { MOD,           'm',           }, { setlayout,         { "[ ]" }              } },
	{ { MOD,           ' ',           }, { setlayout,         { NULL }               } },
	{ { MOD,           'i',           }, { incnmaster,        { "+1" }               } },
	{ { MOD,           'd',           }, { incnmaster,        { "-1" }               } },
	{ { MOD,           'h',           }, { setmfact,          { "-0.05" }            } },
	{ { MOD,           'l',           }, { setmfact,          { "+0.05" }            } },
	{ { MOD,           '.',           }, { toggleminimize,    { NULL }               } },
	{ { MOD,           's',           }, { togglebar,         { NULL }               } },
	{ { MOD,           'S',           }, { togglebarpos,      { NULL }               } },
	{ { MOD,           'M',           }, { togglemouse,       { NULL }               } },
	{ { MOD,           '\n',          }, { zoom ,             { NULL }               } },
	{ { MOD,           '\r',          }, { zoom ,             { NULL }               } },
	{ { MOD,           '1',           }, { focusn,            { "1" }                } },
	{ { MOD,           '2',           }, { focusn,            { "2" }                } },
	{ { MOD,           '3',           }, { focusn,            { "3" }                } },
	{ { MOD,           '4',           }, { focusn,            { "4" }                } },
	{ { MOD,           '5',           }, { focusn,            { "5" }                } },
	{ { MOD,           '6',           }, { focusn,            { "6" }                } },
	{ { MOD,           '7',           }, { focusn,            { "7" }                } },
	{ { MOD,           '8',           }, { focusn,            { "8" }                } },
	{ { MOD,           '9',           }, { focusn,            { "9" }                } },
	{ { MOD,           '\t',          }, { focuslast,         { NULL }               } },
	{ { MOD,           'q', 'q',      }, { quit,              { NULL }               } },
	{ { MOD,           'a',           }, { togglerunall,      { NULL }               } },
	{ { MOD,           CTRL('L'),     }, { redraw,            { NULL }               } },
	{ { MOD,           'r',           }, { redraw,            { NULL }               } },
	{ { MOD,           'e',           }, { copymode,          { NULL }               } },
	{ { MOD,           '/',           }, { copymode,          { "/" }                } },
	{ { MOD,           'p',           }, { paste,             { NULL }               } },
	{ { MOD,           KEY_PPAGE,     }, { scrollback,        { "-1" }               } },
	{ { MOD,           KEY_NPAGE,     }, { scrollback,        { "1"  }               } },
	{ { MOD,           '?',           }, { create,            { "man dvtm", "dvtm help" } } },
	{ { MOD,           MOD,           }, { send,              { (const char []){MOD, 0} } } },
	{ { KEY_SPREVIOUS,                }, { scrollback,        { "-1" }                    } },
	{ { KEY_SNEXT,                    }, { scrollback,        { "1"  }                    } },
	{ { MOD,           '0',           }, { view,              { NULL }                    } },
	{ { MOD,           KEY_F(1),      }, { view,              { tags[0] }                 } },
	{ { MOD,           KEY_F(2),      }, { view,              { tags[1] }                 } },
	{ { MOD,           KEY_F(3),      }, { view,              { tags[2] }                 } },
	{ { MOD,           KEY_F(4),      }, { view,              { tags[3] }                 } },
	{ { MOD,           KEY_F(5),      }, { view,              { tags[4] }                 } },
	{ { MOD,           'v', '0'       }, { view,              { NULL }                    } },
	{ { MOD,           'v', '\t',     }, { viewprevtag,       { NULL }                    } },
	{ { MOD,           't', '0'       }, { tag,               { NULL }                    } },
	TAGKEYS( '1',                              0)
	TAGKEYS( '2',                              1)
	TAGKEYS( '3',                              2)
	TAGKEYS( '4',                              3)
	TAGKEYS( '5',                              4)
};

static const ColorRule colorrules[] = {
	{ "", A_NORMAL, &colors[DEFAULT] }, /* default */
};

/* possible values for the mouse buttons are listed below:
 *
 * BUTTON1_PRESSED          mouse button 1 down
 * BUTTON1_RELEASED         mouse button 1 up
 * BUTTON1_CLICKED          mouse button 1 clicked
 * BUTTON1_DOUBLE_CLICKED   mouse button 1 double clicked
 * BUTTON1_TRIPLE_CLICKED   mouse button 1 triple clicked
 * BUTTON2_PRESSED          mouse button 2 down
 * BUTTON2_RELEASED         mouse button 2 up
 * BUTTON2_CLICKED          mouse button 2 clicked
 * BUTTON2_DOUBLE_CLICKED   mouse button 2 double clicked
 * BUTTON2_TRIPLE_CLICKED   mouse button 2 triple clicked
 * BUTTON3_PRESSED          mouse button 3 down
 * BUTTON3_RELEASED         mouse button 3 up
 * BUTTON3_CLICKED          mouse button 3 clicked
 * BUTTON3_DOUBLE_CLICKED   mouse button 3 double clicked
 * BUTTON3_TRIPLE_CLICKED   mouse button 3 triple clicked
 * BUTTON4_PRESSED          mouse button 4 down
 * BUTTON4_RELEASED         mouse button 4 up
 * BUTTON4_CLICKED          mouse button 4 clicked
 * BUTTON4_DOUBLE_CLICKED   mouse button 4 double clicked
 * BUTTON4_TRIPLE_CLICKED   mouse button 4 triple clicked
 * BUTTON_SHIFT             shift was down during button state change
 * BUTTON_CTRL              control was down during button state change
 * BUTTON_ALT               alt was down during button state change
 * ALL_MOUSE_EVENTS         report all button state changes
 * REPORT_MOUSE_POSITION    report mouse movement
 */

#ifdef NCURSES_MOUSE_VERSION
# define CONFIG_MOUSE /* compile in mouse support if we build against ncurses */
#endif

#define ENABLE_MOUSE true /* whether to enable mouse events by default */

#ifdef CONFIG_MOUSE
static Button buttons[] = {
	{ BUTTON1_CLICKED,        { mouse_focus,      { NULL  } } },
	{ BUTTON1_DOUBLE_CLICKED, { mouse_fullscreen, { "[ ]" } } },
	{ BUTTON2_CLICKED,        { mouse_zoom,       { NULL  } } },
	{ BUTTON3_CLICKED,        { mouse_minimize,   { NULL  } } },
};
#endif /* CONFIG_MOUSE */

static Cmd commands[] = {
	{ "create", { create,	{ NULL } } },
};

/* gets executed when dvtm is started */
static Action actions[] = {
	{ create, { NULL } },
};

static char const * const keytable[] = {
	/* add your custom key escape sequences */
};

/* editor to use for copy mode. If neither of DVTM_EDITOR, EDITOR and PAGER is
 * set the first entry is chosen. Otherwise the array is consulted for supported
 * options. A %d in argv is replaced by the line number at which the file should
 * be opened. If filter is true the editor is expected to work even if stdout is
 * redirected (i.e. not a terminal). If color is true then color escape sequences
 * are generated in the output.
 */
static Editor editors[] = {
	{ .name = "vis",         .argv = { "vis", "+%d", "-", NULL   }, .filter = true,  .color = false },
	{ .name = "sandy",       .argv = { "sandy", "-d", "-", NULL  }, .filter = true,  .color = false },
	{ .name = "dvtm-editor", .argv = { "dvtm-editor", "-", NULL  }, .filter = true,  .color = false },
	{ .name = "vim",         .argv = { "vim", "+%d", "-", NULL   }, .filter = false, .color = false },
	{ .name = "less",        .argv = { "less", "-R", "+%d", NULL }, .filter = false, .color = true  },
	{ .name = "more",        .argv = { "more", "+%d", NULL       }, .filter = false, .color = false },
};
